import java.text.SimpleDateFormat;
import java.util.Date;

public class Sub2 {
    public static void main(String[] args) {
        YThread y1 = new YThread();
        YThread y2 = new YThread();

        Thread t1 = new Thread(y1, "YThread1");
        Thread t2 = new Thread(y2, "YThread2");

        t1.start();
        t2.start();

    }
}

class YThread implements Runnable{

    public void run(){
        Thread t = Thread.currentThread();
        for(int i = 0; i<4; i++){
            SimpleDateFormat formatter = new SimpleDateFormat(" HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String msg = t.getName() + formatter.format(date);
            System.out.println(msg);
            try{
                Thread.sleep(10000);
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(t.getName() + " its job is DONE!");

    }
}
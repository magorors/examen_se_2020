public class Sub1 {
    public static void main(String[] args) {

    }
}

interface U {
    void f();
    void i(J j);

}
class J {

}
class I implements U{

    private long t;
    K k;
    public void f(){
        System.out.println("Implemented f");

    }
    public void i(J j){
        System.out.println("Implemented f with dependence on J");

    }
}

class K{
    S s = new S();
    L l = new L();
    //the difference between composition and aggregation is only phylosophical
    //meaning that when I refer to aggregation, I refer to the fact that the
    //owner HAS A type of class, but without the owner, the owned class can exist!
    //while in case of the COMPOSITION, the owned class is PART OF a WHOLE, and cannot exists
    //without it, that is why, in case of the aggregation, I created also a class in the aggregated one.

}

class N{
    I i;
}

class L{
    public void metA(){}
}

class S{
    S m = new S();
    public void metB(){}
}
